package cz.lyosoft.keto.reader;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;


public class FileKetoReader {
    private static final String FILE_PATH = "./xls/";
    private static final String XLS_HODNOTZ_FILE_NAME = "keto_hodnoty.xlsx";


    /**
     * Nacte soubor
     *
     * @return vraci instanci Woorkbook z poi apache excel
     */
    public Workbook getFile() {
        Workbook ketoValuesSheet = null;
        try {
            // Creating a Workbook from an Excel file (.xls or .xlsx)
            ketoValuesSheet = WorkbookFactory.create(new File(FILE_PATH + XLS_HODNOTZ_FILE_NAME));

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (ketoValuesSheet != null) {
            // Retrieving the number of sheets in the Workbook
            System.out.println("Workbook has " + ketoValuesSheet.getNumberOfSheets() + " Sheets : " + ketoValuesSheet.getSheetName(0));
        }

        return ketoValuesSheet;
    }

}
