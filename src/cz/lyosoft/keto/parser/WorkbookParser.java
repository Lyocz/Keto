package cz.lyosoft.keto.parser;

import cz.lyosoft.keto.model.Surovina;
import cz.lyosoft.keto.reader.FileKetoReader;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.*;

/**
 * Nacte radky do Mapy
 * key - jmeno
 * value - ArrazLIst Hodnot (yachovale poradi sloupcu)
 */

public class WorkbookParser {
    public static final String SUROVINA_NAME = "Surovina";
    public static final String SACHARIDY_NAME = "sacharidy";
    public static final String BILKOVINY_NAME = "bilkoviny";
    public static final String TUKY_NAME = "tuky";
    public static final String NA_MNOZSTS_NAME = "na mnozstvi";
    public static final String CENA_NAME = "cena";
    public static final String ZA_MNOZST_NAME = "za mnozstvi";
    private static final Object lock = new Object();
    private static volatile WorkbookParser instance;
    private FileKetoReader reader;

    private WorkbookParser() {
        reader = new FileKetoReader();
    }

    public static WorkbookParser getInstance() {
        WorkbookParser r = instance;
        if (r == null) {
            synchronized (lock) {    // While we were waiting for the lock, another
                r = instance;        // thread may have instantiated the object.
                if (r == null) {
                    r = new WorkbookParser();
                    instance = r;
                }
            }
        }
        return r;
    }

    /**
     * Nacte hlavni radek do Mapy s jmenz sloupcu
     * key - jmeno sloupce
     * value - cislo sloupce
     */
    protected Map prepareHeaders() {
        Workbook wb = getInstance().reader.getFile();
        Sheet sheet = wb.getSheet(wb.getSheetName(0));
        Row row = sheet.getRow(sheet.getFirstRowNum());
        Map mHeaders = new HashMap();

        Iterator it = row.cellIterator();
        while (it.hasNext()) {
            Cell c = (Cell) it.next();
            c.getColumnIndex();
            mHeaders.put(c.getStringCellValue(), c.getColumnIndex());
        }
        return mHeaders;
    }

    /**
     * Nacte radky do mapy
     * key - jmeno suroviny
     * hodbota - Mapa hodnot jednotlivzch poloyek pro surovinu
     */
    public List<Surovina> parse() {
        List<Surovina> lSuroviny = new ArrayList<Surovina>();
        Workbook wb = getInstance().reader.getFile();
        Map mGetetrs = prepareHeaders();
        Sheet sheet = wb.getSheet(wb.getSheetName(0));

        int dataRow = sheet.getFirstRowNum() + 1;

        for (int i = dataRow; i <= sheet.getLastRowNum(); i++) {

            Row row = sheet.getRow(i);
            Surovina surovina = new Surovina();
            Cell cell = row.getCell(Integer.valueOf(mGetetrs.get(SACHARIDY_NAME).toString()));

            surovina.setNazev(row.getCell(Integer.valueOf(mGetetrs.get(SUROVINA_NAME).toString())).getStringCellValue());
            if (cell != null) {
                surovina.setSacharidy(cell.getNumericCellValue());
            }

            cell = row.getCell(Integer.valueOf(mGetetrs.get(BILKOVINY_NAME).toString()));
            if (cell != null) {
                surovina.setBilkoviny(cell.getNumericCellValue());
            }

            cell = row.getCell(Integer.valueOf(mGetetrs.get(TUKY_NAME).toString()));
            if (cell != null) {
                surovina.setTuky(cell.getNumericCellValue());
            }

            cell = row.getCell(Integer.valueOf(mGetetrs.get(NA_MNOZSTS_NAME).toString()));
            if (cell != null) {
                surovina.setNaMnozstvi(cell.getNumericCellValue());
            }

            cell = row.getCell(Integer.valueOf(mGetetrs.get(CENA_NAME).toString()));
            if (cell != null) {
                surovina.setCena(cell.getNumericCellValue());
            }

            cell = row.getCell(Integer.valueOf(mGetetrs.get(ZA_MNOZST_NAME).toString()));
            if (cell != null) {
                surovina.setZaMnozstvi(cell.getNumericCellValue());
            }

            lSuroviny.add(surovina);


        }

        return lSuroviny;
    }
}