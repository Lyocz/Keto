package cz.lyosoft.keto.parser

class WorkbookParserTest extends GroovyTestCase {

    WorkbookParser parser = WorkbookParser.getInstance();

    void testPrepareColumsGetters() {
        Map getters = parser.prepareHeaders();
        assertNotNull(getters.keySet());

        Iterator it = getters.keySet().iterator();

        while (it.hasNext()) {
            String o = String.valueOf(it.next()).trim();
            println(o + " " + getters.get(o));

        }
    }

    void testParse() {
        Map map = parser.parse();

        assertNotNull(map.keySet());

        Iterator it = map.keySet().iterator();

        while (it.hasNext()) {
            String o = String.valueOf(it.next()).trim();
            Map m = map.get(o);
            println(o + " - " + m.get(WorkbookParser.SACHARIDY_NAME) + ",  " + m.get(WorkbookParser.BILKOVINY_NAME) + ",  " + m.get(WorkbookParser.TUKY_NAME));


        }

    }
}
