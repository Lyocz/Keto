package cz.lyosoft.keto.ui;

import cz.lyosoft.keto.logic.SheetModelDataHandler;
import cz.lyosoft.keto.ui.handlers.MainHandler;

import javax.swing.*;
import java.awt.*;

public class MainFrame {
    JFrame frame;
    SheetModelDataHandler dataHandler;
    MainHandler mainHandler;

    public MainFrame() {
        dataHandler = new SheetModelDataHandler();
        mainHandler = MainHandler.getInstance();
        mainHandler.getSurovinyTabHandler().setTableData(dataHandler.getSuroviny());
        init();
    }

    public static void main(String[] args) {
        MainFrame frame = new MainFrame();
    }

    public void init() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.setContentPane(mainHandler.getMainPane());
        frame.setPreferredSize(new Dimension(1200, 800));
        frame.pack();
        frame.setVisible(true);

    }


}
