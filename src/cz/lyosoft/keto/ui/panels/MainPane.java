package cz.lyosoft.keto.ui.panels;

import javax.swing.*;

public class MainPane {


    private JTabbedPane tabedPaneSuroviny;
    private JPanel mainPane;


    public MainPane() {
    }

    public JTabbedPane getTabedPaneSuroviny() {
        return tabedPaneSuroviny;
    }

    public JPanel getMainPane() {
        return mainPane;
    }
}


