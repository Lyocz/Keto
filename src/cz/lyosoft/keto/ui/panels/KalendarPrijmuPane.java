package cz.lyosoft.keto.ui.panels;

import javax.swing.*;

public class KalendarPrijmuPane {
    JPanel pKalendarPrijmu;
    JScrollPane jspDaysFood;
    JTable tabKalendarPrijmu;
    JButton btnSave;
    JTextField tdDate;
    JLabel lDate;
    JPanel mainPane;
    private JComboBox cmbSavedDays;
    private JButton btnLoad;
    private JButton btnRemove;

    public JTable getTabKalendarPrijmu() {
        return tabKalendarPrijmu;
    }

    public JComboBox getCmbSavedDays() {
        return cmbSavedDays;
    }

    public void setCmbSavedDays(JComboBox cmbSavedDays) {
        this.cmbSavedDays = cmbSavedDays;
    }

    public JButton getBtnLoad() {
        return btnLoad;
    }

    public JButton getBtnSave() {
        return btnSave;
    }

    public JTextField getTdDate() {
        return tdDate;
    }

    public JPanel getMainPane() {
        return mainPane;
    }

    public JButton getBtnRemove() {
        return btnRemove;
    }

    private void createUIComponents() {
        tabKalendarPrijmu = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }
}
