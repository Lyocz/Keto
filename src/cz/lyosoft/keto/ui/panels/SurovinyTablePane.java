package cz.lyosoft.keto.ui.panels;

import javax.swing.*;

public class SurovinyTablePane {

    JScrollPane jscPane;
    JTable tabSuroviny;
    private JPanel mainPane;


    public JTable getTabSuroviny() {
        return tabSuroviny;
    }

    public JPanel getMainPane() {
        return mainPane;
    }

    private void createUIComponents() {
        tabSuroviny = new JTable() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }


}
