package cz.lyosoft.keto.ui.panels;

import javax.swing.*;

public class SurovinyPorcePane {
    JPanel pSurovinyPorce;
    JTextField tdName;
    JTextField tdTuky;
    JTextField tdBilkoviny;
    JTextField tdSacharidy;

    JLabel lTuky;
    JLabel lBilkoviny;
    JLabel lSacharidy;
    JLabel lSurovina;

    JTextField tdGramaz;
    JLabel lGramaz;

    JButton btnCalculate;
    JTextField tdPorceVaha;
    JLabel lPocetGramu;

    JLabel lPorceHodnoty;

    JTextField tdTukyNaPorci;
    JTextField tdBilkovinyNaPorci;
    JTextField tdSacharidyNaPorci;

    JButton btnAdd;
    JPanel mainPane;
    private JLabel lNotice;

    public JTextField getTdName() {
        return tdName;
    }

    public JTextField getTdTuky() {
        return tdTuky;
    }

    public JTextField getTdBilkoviny() {
        return tdBilkoviny;
    }

    public JTextField getTdSacharidy() {
        return tdSacharidy;
    }

    public JTextField getTdGramaz() {
        return tdGramaz;
    }

    public JButton getBtnCalculate() {
        return btnCalculate;
    }

    public JTextField getTdPorceVaha() {
        return tdPorceVaha;
    }

    public JTextField getTdTukyNaPorci() {
        return tdTukyNaPorci;
    }

    public JTextField getTdBilkovinyNaPorci() {
        return tdBilkovinyNaPorci;
    }

    public JTextField getTdSacharidyNaPorci() {
        return tdSacharidyNaPorci;
    }

    public JButton getBtnAdd() {
        return btnAdd;
    }

    public JPanel getMainPane() {
        return mainPane;
    }


    public JLabel getlNotice() {
        return lNotice;
    }
}
