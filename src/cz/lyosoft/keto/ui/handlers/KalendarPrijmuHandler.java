package cz.lyosoft.keto.ui.handlers;

import cz.lyosoft.keto.logic.json.JsonWriter;
import cz.lyosoft.keto.logic.util.DateTimeUtils;
import cz.lyosoft.keto.model.Den;
import cz.lyosoft.keto.model.KalendarPrijmu;
import cz.lyosoft.keto.model.Porce;
import cz.lyosoft.keto.model.Surovina;
import cz.lyosoft.keto.ui.panels.KalendarPrijmuPane;
import cz.lyosoft.keto.ui.utils.HandlersUtils;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class KalendarPrijmuHandler {
    public static final String BTN_SAVE = "SAVE";
    public static final String BTN_LOAD = "LOAD";
    public static final String BTN_REMOVE = "REMOVE";
    private static KalendarPrijmuHandler instance;
    KalendarPrijmuPane kalendarPrijmuPane;
    private KalendarPrijmu kalendarPrijmu;
    private static final Object lock = new Object();

    private KalendarPrijmuHandler() {
        init();
    }

    public static KalendarPrijmuHandler getInstance() {
        KalendarPrijmuHandler r = instance;
        if (r == null) {
            synchronized (lock) {    // While we were waiting for the lock, another
                r = instance;        // thread may have instantiated the object.
                if (r == null) {
                    r = new KalendarPrijmuHandler();
                    instance = r;
                }
            }
        }
        return r;
    }

    private void init() {
        kalendarPrijmu = new KalendarPrijmu();

        kalendarPrijmuPane = new KalendarPrijmuPane();
        kalendarPrijmuPane.getTabKalendarPrijmu().setName("Denni prijem");
        ACL acl = new ACL();

        kalendarPrijmuPane.getBtnSave().addActionListener(acl);
        kalendarPrijmuPane.getBtnSave().setActionCommand(BTN_SAVE);
        kalendarPrijmuPane.getBtnLoad().addActionListener(acl);
        kalendarPrijmuPane.getBtnLoad().setActionCommand(BTN_LOAD);
        kalendarPrijmuPane.getBtnRemove().addActionListener(acl);
        kalendarPrijmuPane.getBtnRemove().setActionCommand(BTN_REMOVE);
        kalendarPrijmuPane.getCmbSavedDays().addItemListener(acl);

        kalendarPrijmuPane.getTabKalendarPrijmu().addMouseListener(new ML());

        prepareColums();
        initCmb();
        kalendarPrijmuPane.getCmbSavedDays().setSelectedItem(DateTimeUtils.getToday());
        kalendarPrijmuPane.getBtnRemove().setEnabled(false);
    }


    private void prepareColums() {
        ((DefaultTableModel) kalendarPrijmuPane.getTabKalendarPrijmu().getModel()).addColumn(SurovinyTableHandler.NAZEV);
        ((DefaultTableModel) kalendarPrijmuPane.getTabKalendarPrijmu().getModel()).addColumn(SurovinyTableHandler.TUKY);
        ((DefaultTableModel) kalendarPrijmuPane.getTabKalendarPrijmu().getModel()).addColumn(SurovinyTableHandler.BILKOVINY);
        ((DefaultTableModel) kalendarPrijmuPane.getTabKalendarPrijmu().getModel()).addColumn(SurovinyTableHandler.SACHARIDY);
        ((DefaultTableModel) kalendarPrijmuPane.getTabKalendarPrijmu().getModel()).setColumnCount(4);
    }


    public void addPorci(Surovina surovina) {
        kalendarPrijmu.getToday().addPorce(surovina.getPorce());
    }

    public void loadDay(Den den) {
        clearTableData();
        Surovina surovina = new Surovina();
        for (Porce p : den.getPorces()) {
            surovina.setPorce(p);
            HandlersUtils.insertRowData(kalendarPrijmuPane.getTabKalendarPrijmu(), surovina, true);
        }
    }


    public void initCmb() {
        JComboBox cmb = kalendarPrijmuPane.getCmbSavedDays();
        for (Den registeredDays : kalendarPrijmu.getZpracovaneDny()) {
            cmb.addItem(registeredDays.getDatum().toString(DateTimeUtils.DATE_FORMAT));
        }
    }

    public void clearTableData() {
        ((DefaultTableModel) kalendarPrijmuPane.getTabKalendarPrijmu().getModel()).getDataVector().removeAllElements();
        kalendarPrijmuPane.getTabKalendarPrijmu().revalidate();
    }


    public List<Porce> getPorceFromDaysTable() {
        DefaultTableModel model = (DefaultTableModel) kalendarPrijmuPane.getTabKalendarPrijmu().getModel();
        List<Porce> lPorce = new ArrayList<>();

        for (int row = 0; row < model.getRowCount(); row++) {
            Enumeration columns = kalendarPrijmuPane.getTabKalendarPrijmu().getColumnModel().getColumns();
            Porce porce = new Porce();
            while (columns.hasMoreElements()) {
                Object col = columns.nextElement();
                String header = (String) ((TableColumn) col).getHeaderValue();


                String value = String.valueOf(kalendarPrijmuPane.getTabKalendarPrijmu().getModel().getValueAt(row, kalendarPrijmuPane.getTabKalendarPrijmu().getColumnModel().getColumnIndex(header)));
                switch (header) {
                    case SurovinyTableHandler.NAZEV:
                        porce.setNazev(value);
                        break;
                    case SurovinyTableHandler.BILKOVINY:
                        porce.setBilkoviny(HandlersUtils.parseDouble(value));
                        break;
                    case SurovinyTableHandler.TUKY:
                        porce.setTuky(HandlersUtils.parseDouble(value));
                        break;
                    case SurovinyTableHandler.SACHARIDY:
                        porce.setSacharidy(HandlersUtils.parseDouble(value));
                        break;
                    case SurovinyPorceHandler.GRAMY:
                        porce.setGramy(HandlersUtils.parseDouble(value));
                        break;
                }
            }

            lPorce.add(porce);
        }

        return lPorce;
    }


    public JPanel getMainPane() {
        return kalendarPrijmuPane.getMainPane();
    }


    class ACL implements ActionListener, ItemListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals(KalendarPrijmuHandler.BTN_REMOVE)) {
                ((DefaultTableModel) kalendarPrijmuPane.getTabKalendarPrijmu().getModel()).removeRow(kalendarPrijmuPane.getTabKalendarPrijmu().getSelectedRow());
                kalendarPrijmuPane.getBtnRemove().setEnabled(false);

            } else if (e.getActionCommand().equals(KalendarPrijmuHandler.BTN_SAVE)) {
                List<Porce> porce = getPorceFromDaysTable();
                kalendarPrijmu.getDen(kalendarPrijmuPane.getCmbSavedDays().getSelectedItem().toString()).setPorces(porce);
                JsonWriter.save(kalendarPrijmu);
            } else if (e.getActionCommand().equals(KalendarPrijmuHandler.BTN_LOAD)) {
                loadDay(kalendarPrijmu.getDen(kalendarPrijmuPane.getCmbSavedDays().getSelectedItem().toString()));
            }
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            System.out.println("Vybrano: " + e.getItem());
            for (Den d : kalendarPrijmu.getZpracovaneDny()) {
                if (DateTimeUtils.formatedDateString(d.getDatum()).equals(kalendarPrijmuPane.getCmbSavedDays().getSelectedItem())) {
                    loadDay(d);
                }
            }
        }
    }

    class ML implements MouseListener {
        @Override
        public void mouseClicked(MouseEvent e) {
            JTable table = (JTable) e.getSource();
            int selectedRow = table.rowAtPoint(e.getPoint());
            kalendarPrijmuPane.getBtnRemove().setEnabled(true);

        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }


}
