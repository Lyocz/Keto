package cz.lyosoft.keto.ui.handlers;

import cz.lyosoft.keto.ui.panels.MainPane;

import javax.swing.*;

public class MainHandler {
    public static final String SUROVINY_TAB_NAME = "Suroviny";
    public static final String SUROVINA_PORCE_NAME = "Porce";
    public static final String PRIJEM_TAB_NAME = "Dnešní příjem";

    private static MainHandler instance;
    private static Object lock = new Object();

    MainPane mainPane;
    SurovinyTableHandler hSurovinyTab;
    SurovinyPorceHandler hSurovinyPorce;
    KalendarPrijmuHandler hKalendarPrijmu;


    private MainHandler() {
        mainPane = new MainPane();
        mainPane.getTabedPaneSuroviny();

        hSurovinyTab = SurovinyTableHandler.getInstance();
        hSurovinyPorce = SurovinyPorceHandler.getInstance();
        hKalendarPrijmu = KalendarPrijmuHandler.getInstance();

        mainPane.getTabedPaneSuroviny().addTab(SUROVINY_TAB_NAME, hSurovinyTab.getMainPane());
        mainPane.getTabedPaneSuroviny().addTab(SUROVINA_PORCE_NAME, hSurovinyPorce.getMainPane());
        mainPane.getTabedPaneSuroviny().addTab(PRIJEM_TAB_NAME, hKalendarPrijmu.getMainPane());

    }

    public static MainHandler getInstance() {
        MainHandler r = instance;
        if (r == null) {
            synchronized (lock) {    // While we were waiting for the lock, another
                r = instance;        // thread may have instantiated the object.
                if (r == null) {
                    r = new MainHandler();
                    instance = r;
                }
            }
        }
        return r;
    }

    public JPanel getMainPane() {
        return mainPane.getMainPane();
    }


    public SurovinyTableHandler getSurovinyTabHandler() {
        return hSurovinyTab;
    }

}
