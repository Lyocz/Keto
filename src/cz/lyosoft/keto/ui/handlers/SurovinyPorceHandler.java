package cz.lyosoft.keto.ui.handlers;

import cz.lyosoft.keto.logic.PorceManager;
import cz.lyosoft.keto.model.Porce;
import cz.lyosoft.keto.model.Surovina;
import cz.lyosoft.keto.ui.panels.SurovinyPorcePane;
import cz.lyosoft.keto.ui.utils.HandlersUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SurovinyPorceHandler {

    public static final String COUNT_BUTTON_COMMAND = "COUNT";
    public static final String ADD_BUTTON_COMMAND = "ADD";
    public static final String GRAMY = "Gramy";
    private static SurovinyPorceHandler instance;
    private static Object lock = new Object();
    private SurovinyPorcePane surovinyPorcePane;

    private SurovinyPorceHandler() {
        init();
    }

    public static SurovinyPorceHandler getInstance() {
        SurovinyPorceHandler r = instance;
        if (r == null) {
            synchronized (lock) {    // While we were waiting for the lock, another
                r = instance;        // thread may have instantiated the object.
                if (r == null) {
                    r = new SurovinyPorceHandler();
                    instance = r;
                }
            }
        }
        return r;
    }

    private void init() {
        surovinyPorcePane = new SurovinyPorcePane();
        ButtonListener bListener = new ButtonListener();
        surovinyPorcePane.getBtnCalculate().addActionListener(bListener);
        surovinyPorcePane.getBtnCalculate().setActionCommand(COUNT_BUTTON_COMMAND);

        surovinyPorcePane.getBtnAdd().addActionListener(bListener);
        surovinyPorcePane.getBtnAdd().setActionCommand(ADD_BUTTON_COMMAND);
        surovinyPorcePane.getBtnAdd().setEnabled(false);

    }

    public SurovinyPorcePane getSurovinyPorcePane() {
        return surovinyPorcePane;
    }

    public JPanel getMainPane() {
        return surovinyPorcePane.getMainPane();
    }

    public void resetFields() {
        surovinyPorcePane.getTdTuky().setText("");
        surovinyPorcePane.getTdTukyNaPorci().setText("");
        surovinyPorcePane.getTdBilkoviny().setText("");
        surovinyPorcePane.getTdBilkovinyNaPorci().setText("");
        surovinyPorcePane.getTdSacharidy().setText("");
        surovinyPorcePane.getTdSacharidyNaPorci().setText("");
        surovinyPorcePane.getTdName().setText("");
        surovinyPorcePane.getTdGramaz().setText("");
        surovinyPorcePane.getTdPorceVaha().setText("");
    }

    class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            if (e.getActionCommand().equals(SurovinyPorceHandler.COUNT_BUTTON_COMMAND)) {
                String vahaText = surovinyPorcePane.getTdPorceVaha().getText();
                String name = surovinyPorcePane.getTdName().getText();

                if (name == null || name.isEmpty()) {
                    surovinyPorcePane.getlNotice().setForeground(Color.RED);
                    surovinyPorcePane.getlNotice().setText("Vyberte Surovinu");

                    return;
                }

                if (vahaText == null || vahaText.isEmpty()) {
                    surovinyPorcePane.getlNotice().setForeground(Color.RED);
                    surovinyPorcePane.getlNotice().setText("Zadejte vahu");
                    return;
                }


                PorceManager pManager = new PorceManager();
                Surovina surovina = new Surovina();

                // Nacteni hodnot z oznaceneho radku


                // naplneni suroviny
                surovina.setNazev(surovinyPorcePane.getTdName().getText());
                surovina.setTuky(HandlersUtils.parseDouble(surovinyPorcePane.getTdTuky().getText()));
                surovina.setBilkoviny(HandlersUtils.parseDouble(surovinyPorcePane.getTdBilkoviny().getText()));
                surovina.setSacharidy(HandlersUtils.parseDouble(surovinyPorcePane.getTdSacharidy().getText()));
                surovina.setNaMnozstvi(HandlersUtils.parseDouble(surovinyPorcePane.getTdGramaz().getText()));


                if (surovina.getNaMnozstvi() == 0) {
                    surovinyPorcePane.getlNotice().setForeground(Color.RED);
                    surovinyPorcePane.getlNotice().setText("Váha není evidovaná");
                    return;
                }

                // Vzpocet Porce
                surovina = pManager.vypocitatPorci(surovina, Double.parseDouble(vahaText));

                surovina.toString();

                surovinyPorcePane.getTdTukyNaPorci().setText(HandlersUtils.formatDouble(surovina.getPorce().getTuky()));
                surovinyPorcePane.getTdBilkovinyNaPorci().setText(HandlersUtils.formatDouble(surovina.getPorce().getBilkoviny()));
                surovinyPorcePane.getTdSacharidyNaPorci().setText(HandlersUtils.formatDouble(surovina.getPorce().getSacharidy()));
                surovinyPorcePane.getBtnAdd().setEnabled(true);


            }

            if (e.getActionCommand().equals(SurovinyPorceHandler.ADD_BUTTON_COMMAND)) {
                Surovina surovina = new Surovina();
                Porce porce = new Porce();
                surovina.setNazev(surovinyPorcePane.getTdName().getText());

                String porceTuky = surovinyPorcePane.getTdTukyNaPorci().getText();
                String porceBilkoviny = surovinyPorcePane.getTdBilkovinyNaPorci().getText();
                String porceSacharidy = surovinyPorcePane.getTdSacharidyNaPorci().getText();

                if (porceTuky == null || porceTuky.isEmpty() ||
                        porceBilkoviny == null || porceBilkoviny.isEmpty() ||
                        porceSacharidy == null || porceSacharidy.isEmpty()) {
                    surovinyPorcePane.getlNotice().setForeground(Color.RED);
                    surovinyPorcePane.getlNotice().setText("Porce nemuze byt pridana, zrejme chybi nejaka data");
                    return;
                }
                porce.setTuky(HandlersUtils.parseDouble(porceTuky));
                porce.setBilkoviny(HandlersUtils.parseDouble(porceBilkoviny));
                porce.setSacharidy(HandlersUtils.parseDouble(porceSacharidy));
                porce.setNazev(surovina.getNazev());

                surovina.setPorce(porce);


                HandlersUtils.insertRowData(KalendarPrijmuHandler.getInstance().kalendarPrijmuPane.getTabKalendarPrijmu(), surovina, true);

                float[] hsbValues = new float[3];
                hsbValues = Color.RGBtoHSB(0, 102, 0, hsbValues);

                surovinyPorcePane.getlNotice().setForeground(Color.getHSBColor(hsbValues[0], hsbValues[1], hsbValues[2]));
                surovinyPorcePane.getlNotice().setText("Uspesne pridano");
                resetFields();

            }
        }
    }
}
