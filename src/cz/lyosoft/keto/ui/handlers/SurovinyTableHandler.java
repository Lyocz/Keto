package cz.lyosoft.keto.ui.handlers;

import cz.lyosoft.keto.model.Surovina;
import cz.lyosoft.keto.ui.panels.SurovinyTablePane;
import cz.lyosoft.keto.ui.utils.HandlersUtils;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

public class SurovinyTableHandler {

    public static final String NAZEV = "Název";
    public static final String TUKY = "Tuky";
    public static final String BILKOVINY = "Bílkoviny";
    public static final String SACHARIDY = "Sacharidy";
    public static final String NA_MNOZSTVI = "Na množství";


    private static volatile SurovinyTableHandler instance;
    SurovinyTablePane surovinyTablePane;
    private static final Object lock = new Object();


    private SurovinyTableHandler() {
        init();
    }

    public static SurovinyTableHandler getInstance() {
        SurovinyTableHandler r = instance;
        if (r == null) {
            synchronized (lock) {    // While we were waiting for the lock, another
                r = instance;        // thread may have instantiated the object.
                if (r == null) {
                    r = new SurovinyTableHandler();
                    instance = r;
                }
            }
        }
        return r;
    }

    private void init() {
        surovinyTablePane = new SurovinyTablePane();
        surovinyTablePane.getTabSuroviny().setName("Suroviny table");

        surovinyTablePane.getTabSuroviny().addMouseListener(new ML());
        prepareColums();
    }

    public JTable getSurovinyTable() {
        return surovinyTablePane.getTabSuroviny();
    }

    private void prepareColums() {
        ((DefaultTableModel) surovinyTablePane.getTabSuroviny().getModel()).addColumn(SurovinyTableHandler.NAZEV);
        ((DefaultTableModel) surovinyTablePane.getTabSuroviny().getModel()).addColumn(SurovinyTableHandler.TUKY);
        ((DefaultTableModel) surovinyTablePane.getTabSuroviny().getModel()).addColumn(SurovinyTableHandler.BILKOVINY);
        ((DefaultTableModel) surovinyTablePane.getTabSuroviny().getModel()).addColumn(SurovinyTableHandler.SACHARIDY);
        //((DefaultTableModel) surovinyMainPane.getTabSuroviny().getModel()).addColumn(SurovinyTableHandler.CENA);
        ((DefaultTableModel) surovinyTablePane.getTabSuroviny().getModel()).addColumn(SurovinyTableHandler.NA_MNOZSTVI);
        //((DefaultTableModel) surovinyMainPane.getTabSuroviny().getModel()).addColumn(SurovinyTableHandler.ZA_MNOZSTVI);
        ((DefaultTableModel) surovinyTablePane.getTabSuroviny().getModel()).setColumnCount(5);

        surovinyTablePane.getTabSuroviny().getColumnModel().getColumn(surovinyTablePane.getTabSuroviny().getColumn(NAZEV).getModelIndex()).setCellRenderer(new BoldCellRenderer());


    }

    public JPanel getMainPane() {
        return surovinyTablePane.getMainPane();
    }

    public void setTableData(List<Surovina> suroviny) {
        HandlersUtils.setTableData(surovinyTablePane.getTabSuroviny(), suroviny);
    }


    class BoldCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
                                                       int column) {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);


            if (table.getColumnName(column).equals(SurovinyTableHandler.NAZEV)) {

            } else if (table.getColumnName(column).equals(SurovinyTableHandler.TUKY)) {
//            value = String.valueOf(value).substring(0,value.toString().indexOf('.'));

            } else if (table.getColumnName(column).equals(SurovinyTableHandler.BILKOVINY)) {

            } else if (table.getColumnName(column).equals(SurovinyTableHandler.BILKOVINY)) {

            } else if (table.getColumnName(column).equals(SurovinyTableHandler.SACHARIDY)) {

            }

            this.setValue(value);
            this.setFont(this.getFont().deriveFont(Font.BOLD));
            this.setHorizontalAlignment(JLabel.RIGHT);

            return this;
        }
    }

    class ML implements MouseListener {
        @Override
        public void mouseClicked(MouseEvent e) {
            JTable table = (JTable) e.getSource();
            int selectedRow = table.rowAtPoint(e.getPoint());

            // Nacteni hodnot z oznaceneho radku

            String name = (String) table.getValueAt(selectedRow, table.getColumnModel().getColumnIndex(SurovinyTableHandler.NAZEV));
            Double tuky = HandlersUtils.parseDouble(table.getModel().getValueAt(selectedRow, table.getColumnModel().getColumnIndex(SurovinyTableHandler.TUKY)) + "");
            Double bilkoviny = HandlersUtils.parseDouble(table.getModel().getValueAt(selectedRow, table.getColumnModel().getColumnIndex(SurovinyTableHandler.BILKOVINY)) + "");
            Double sacharidy = HandlersUtils.parseDouble(table.getModel().getValueAt(selectedRow, table.getColumnModel().getColumnIndex(SurovinyTableHandler.SACHARIDY)) + "");
            Double naMnozstvi = HandlersUtils.parseDouble(table.getModel().getValueAt(selectedRow, table.getColumnModel().getColumnIndex(SurovinyTableHandler.NA_MNOZSTVI)) + "");

            // Zobrazeni udaju
            SurovinyPorceHandler.getInstance().getSurovinyPorcePane().getTdName().setText(name);
            SurovinyPorceHandler.getInstance().getSurovinyPorcePane().getTdTuky().setText(HandlersUtils.formatDouble(tuky));
            SurovinyPorceHandler.getInstance().getSurovinyPorcePane().getTdBilkoviny().setText(HandlersUtils.formatDouble(bilkoviny));
            SurovinyPorceHandler.getInstance().getSurovinyPorcePane().getTdSacharidy().setText(HandlersUtils.formatDouble(sacharidy));
            SurovinyPorceHandler.getInstance().getSurovinyPorcePane().getTdGramaz().setText(HandlersUtils.formatDouble(naMnozstvi));

            System.out.println("Vybrano: " + name);

        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
