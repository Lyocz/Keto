package cz.lyosoft.keto.ui.utils;

import cz.lyosoft.keto.logic.util.MainUtils;
import cz.lyosoft.keto.model.Surovina;
import cz.lyosoft.keto.ui.handlers.SurovinyPorceHandler;
import cz.lyosoft.keto.ui.handlers.SurovinyTableHandler;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

public class HandlersUtils {

    public static void insertRowData(JTable table, Surovina sur) {
        insertRowData(table, sur, false);
    }

    public static void insertRowData(JTable table, Surovina sur, boolean isPorce) {

        Enumeration enm = table.getColumnModel().getColumns();
        Object[] pole = new Object[MainUtils.size(enm)];
        enm = table.getColumnModel().getColumns();

        while (enm.hasMoreElements()) {
            Object col = enm.nextElement();
            String header = (String) ((TableColumn) col).getHeaderValue();
            switch (header) {
                case SurovinyTableHandler.NAZEV:
                    pole[((TableColumn) col).getModelIndex()] = isPorce ? sur.getPorce().getNazev() : sur.getNazev();
                    break;
                case SurovinyTableHandler.BILKOVINY:
                    pole[((TableColumn) col).getModelIndex()] = isPorce ? formatDouble(sur.getPorce().getBilkoviny()) : formatDouble(sur.getBilkoviny());
                    break;
                case SurovinyTableHandler.TUKY:
                    pole[((TableColumn) col).getModelIndex()] = isPorce ? formatDouble(sur.getPorce().getTuky()) : formatDouble(sur.getTuky());
                    break;
                case SurovinyTableHandler.SACHARIDY:
                    pole[((TableColumn) col).getModelIndex()] = isPorce ? formatDouble(sur.getPorce().getSacharidy()) : formatDouble(sur.getSacharidy());
                    break;
                case SurovinyTableHandler.NA_MNOZSTVI:
                    pole[((TableColumn) col).getModelIndex()] = formatDouble(sur.getNaMnozstvi());
                    break;
                case SurovinyPorceHandler.GRAMY:
                    pole[((TableColumn) col).getModelIndex()] = formatDouble(sur.getPorce().getGramy());
                    break;

            }
        }
        ((DefaultTableModel) table.getModel()).addRow(pole);
        ((DefaultTableModel) table.getModel()).fireTableDataChanged();
        System.out.println("Pridavam surovinu: " + (isPorce ? sur.getPorce().getNazev() : sur.getNazev()) + " do tabulky: " + table.getName());
        System.out.println("Pocet radku v tabulce: " + table.getModel().getRowCount());

    }


    public static void setTableData(JTable table, List<Surovina> list) {

        for (Surovina aList : list) {
            HandlersUtils.insertRowData(table, aList);
        }

    }

    public static String formatDouble(Double d) {
        DecimalFormat df = new DecimalFormat("####0.00");

        System.out.println("Value: " + df.format(d));
        return df.format(d);
    }

    public static Double parseDouble(String s) {
        Double value = null;
        NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
        try {
            value = nf.parse(s).doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return value;
    }
}
