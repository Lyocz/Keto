package cz.lyosoft.keto.logic;

import cz.lyosoft.keto.model.Surovina;
import cz.lyosoft.keto.parser.WorkbookParser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Nacte data z excelu, drzi je v pometi
 */
public class SheetModelDataHandler {

    List data = null;

    public SheetModelDataHandler() {
        this.data = WorkbookParser.getInstance().parse();
    }

    /**
     * Nacte data do mapy
     * klic - nazev
     * hodnota HashMap
     */
    public List<Surovina> getSuroviny() {
        return data;
    }

    public List getSurovinyNames() {
        Iterator it = data.iterator();
        List suroviny = new ArrayList();
        while (it.hasNext()) {
            Surovina sur = (Surovina) it.next();
            suroviny.add(sur.getNazev());
        }

        return suroviny;
    }
}
