package cz.lyosoft.keto.logic.json;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class JsonReader {


    /**
     * @param dateString -- format Stringu ddMMyyyy -> 25082018
     * @return precteny Den
     */
    public static JSONObject readDen(String dateString) {
        return read(JsonWorker.JSON_DEN_PRIJMU_FILE_NAME + "_" + dateString);
    }

    public static JSONObject readKalendarPrijmu() {
        return read(JsonWorker.JSON_KALENDAR_PRIJMU_FILE_NAME);
    }


    protected static JSONObject read(String fileName) {
        File file = new File(JsonWorker.getPath() + "\\" + fileName + ".json");
        JSONObject jsonObject = null;
        if (file.exists()) {
            System.out.println("mam soubor");
            try {
                jsonObject = (JSONObject) new JSONParser().parse(new FileReader(file));
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }


        }
        return jsonObject;
    }

}
