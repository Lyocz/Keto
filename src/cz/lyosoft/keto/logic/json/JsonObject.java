package cz.lyosoft.keto.logic.json;

import org.json.simple.JSONObject;

public interface JsonObject {

    JSONObject toJSONObject();

    boolean fromJson(JSONObject jsonObject);
}
