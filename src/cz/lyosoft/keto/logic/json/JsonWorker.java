package cz.lyosoft.keto.logic.json;

import cz.lyosoft.keto.model.Surovina;
import org.json.simple.JSONObject;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class JsonWorker {


    public static final String JSON_DEN_PRIJMU_FILE_NAME = "Den";
    public static final String JSON_KALENDAR_PRIJMU_FILE_NAME = "KalendarPrijmu";


    public static final String getPath() {
        String userHome = System.getProperty("user.home");
        Path defaultSaveTo = Paths.get(userHome, "Documents", "Keto");
        return defaultSaveTo.toString();
    }

    public static List<JSONObject> convert(List<Surovina> lSurovin) {
        List<JSONObject> lJson = new ArrayList<>();
        for (Surovina s : lSurovin) {
            lJson.add(s.toJSONObject());
        }
        return lJson;
    }

}
