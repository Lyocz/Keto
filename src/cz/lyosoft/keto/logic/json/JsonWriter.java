package cz.lyosoft.keto.logic.json;

import cz.lyosoft.keto.model.Den;
import cz.lyosoft.keto.model.KalendarPrijmu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class JsonWriter {


    public static boolean save(Den den) {
        return save(den.toJSONObject().toJSONString(), JsonWorker.JSON_DEN_PRIJMU_FILE_NAME + "_" + den.getDatum().toString("ddMMyyyy"));
    }


    public static boolean save(KalendarPrijmu den) {
        return save(den.toJSONObject().toJSONString(), JsonWorker.JSON_KALENDAR_PRIJMU_FILE_NAME);
    }

    public static boolean save(String jsonString, String fileName) {
        boolean result = false;
        try {
            File file = new File(JsonWorker.getPath() + "\\" + fileName + ".json");
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileWriter fWriter = new FileWriter(file);

            fWriter.write(jsonString);
            fWriter.close();
            System.out.println("Successfully Copied JSON Object to File...");
            System.out.println("\nJSON Object: " + jsonString);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
