package cz.lyosoft.keto.logic;

import cz.lyosoft.keto.model.Porce;
import cz.lyosoft.keto.model.Surovina;

public class PorceManager {

    public Surovina vypocitatPorci(Surovina surovina, double gramy) {
        Porce porce = new Porce();

        // Vypocitani hodnot
        porce.setTuky(surovina.getTuky() * (gramy / surovina.getNaMnozstvi()));
        porce.setBilkoviny(surovina.getBilkoviny() * (gramy / surovina.getNaMnozstvi()));
        porce.setSacharidy(surovina.getSacharidy() * (gramy / surovina.getNaMnozstvi()));

        porce.setGramy(gramy);
        surovina.setPorce(porce);
        return surovina;
    }

}
