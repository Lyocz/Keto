package cz.lyosoft.keto.logic.util;

import java.util.Enumeration;

public class MainUtils {
    public static int size(Enumeration en) {
        int size = 0;
        while (en.hasMoreElements()) {
            Object o = en.nextElement();
            size++;
        }
        return size;
    }
}
