package cz.lyosoft.keto.logic.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateTimeUtils {
    public static final String DATE_FORMAT = "dd.MM.yyyy";

    public static DateTime getDateTime(String stDate) {
        return getDateTime(stDate, DATE_FORMAT);
    }

    public static DateTime getDateTime(String stDate, String format) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
        return new DateTime(formatter.parseDateTime(stDate));

    }

    public static String formatedDateString(DateTime date) {
        return date.toString(DATE_FORMAT);
    }

    public static String getToday() {
        return formatedDateString(new DateTime());
    }
}
