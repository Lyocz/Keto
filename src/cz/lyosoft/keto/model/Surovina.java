package cz.lyosoft.keto.model;

import cz.lyosoft.keto.logic.json.JsonObject;
import org.json.simple.JSONObject;

public class Surovina implements JsonObject {
    protected String nazev;
    protected double tuky;
    protected double bilkoviny;
    protected double sacharidy;
    protected double naMnozstvi;
    protected double cena;
    protected double zaMnozstvi;

    protected Porce porce;

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public double getTuky() {
        return tuky;
    }

    public void setTuky(double tuky) {
        this.tuky = tuky;
    }

    public double getBilkoviny() {
        return bilkoviny;
    }

    public void setBilkoviny(double bilkoviny) {
        this.bilkoviny = bilkoviny;
    }

    public double getSacharidy() {
        return sacharidy;
    }

    public void setSacharidy(double sacharidy) {
        this.sacharidy = sacharidy;
    }

    public double getNaMnozstvi() {
        return naMnozstvi;
    }

    public void setNaMnozstvi(double naMnozstvi) {
        this.naMnozstvi = naMnozstvi;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public double getZaMnozstvi() {
        return zaMnozstvi;
    }

    public void setZaMnozstvi(double zaMnozstvi) {
        this.zaMnozstvi = zaMnozstvi;
    }

    public Porce getPorce() {
        return porce;
    }

    public void setPorce(Porce porce) {
        this.porce = porce;
    }

    public String toString() {
        StringBuilder build = new StringBuilder();
        build.append(" ----------Surovina " + getNazev() + " -------------- ");
        build.append("Tuky: " + getTuky());
        build.append("Bilkoviny: " + getBilkoviny());
        build.append("Sacharidy: " + getSacharidy());
        build.append("Na mnozstvi: " + getNaMnozstvi());

        if (hasPorci()) {
            build.append("Gramy porce: " + porce.getGramy());
            build.append("Tuky na porci: " + porce.getTuky());
            build.append("Bilkoviny na porci: " + porce.getBilkoviny());
            build.append("Sacharidy na porci: " + porce.getSacharidy());
        }
        return build.toString();
    }

    public boolean hasPorci() {
        return getPorce() != null && getPorce().getTuky() != 0 && getPorce().getBilkoviny() != 0 && getPorce().getSacharidy() != 0;
    }


    public boolean hasData() {
        return getTuky() != 0 && getBilkoviny() != 0 && getSacharidy() != 0 && getNaMnozstvi() != 0;
    }


    /* ----------------    Json Part -------------------------------------------------- */


    @Override
    public JSONObject toJSONObject() {

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("nazev", getNazev());
        jsonObj.put("tuky", getTuky());
        jsonObj.put("bilkoviny", getBilkoviny());
        jsonObj.put("sacharidy", getSacharidy());
        jsonObj.put("naMnozstvi", getNaMnozstvi());

        if (hasPorci()) {
            jsonObj.put("porce", porce.toJSONObject());
        }
        return jsonObj;
    }

    @Override
    public boolean fromJson(JSONObject jsonObject) {
        return false;
    }
}