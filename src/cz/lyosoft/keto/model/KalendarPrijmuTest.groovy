package cz.lyosoft.keto.model

import cz.lyosoft.keto.logic.json.JsonReader
import cz.lyosoft.keto.logic.json.JsonWriter
import cz.lyosoft.keto.logic.util.DateTimeUtils
import cz.lyosoft.keto.model.Den
import cz.lyosoft.keto.model.KalendarPrijmu
import cz.lyosoft.keto.model.Porce
import org.json.simple.JSONObject

class KalendarPrijmuTest extends GroovyTestCase {

    KalendarPrijmu kalPrijmu = null;
    Den den = null;

    KalendarPrijmuTest() {
        kalPrijmu = new KalendarPrijmu();
        kalPrijmu.clearZpracovaneDny();

        den = new Den();
        den.setDatum(DateTimeUtils.getDateTime("01.09.2018"));

        Porce porce = new Porce();
        porce.setNazev("Porce 1");
        porce.setTuky(10.0);
        porce.setBilkoviny(8.0);
        porce.setSacharidy(6.0);
        porce.setGramy(20.0)
        den.addPorce(porce);

        // druha porce
        porce = new Porce();
        porce.setNazev("Porce 2");
        porce.setTuky(8.0);
        porce.setBilkoviny(4.0);
        porce.setSacharidy(2.0);
        porce.setGramy(40.0)
        den.addPorce(porce);

        kalPrijmu.addDen(den);

        den = new Den();
        den.setDatum(DateTimeUtils.getDateTime("10.09.2018"));

        porce = new Porce();
        porce.setNazev("Porce 1");
        porce.setTuky(5.0);
        porce.setBilkoviny(4.0);
        porce.setSacharidy(3.0);
        porce.setGramy(10.0)
        den.addPorce(porce);

        // druha porce
        porce = new Porce();
        porce.setNazev("Porce 2");
        porce.setTuky(8.0);
        porce.setBilkoviny(4.0);
        porce.setSacharidy(2.0);
        porce.setGramy(40.0)
        den.addPorce(porce);
        kalPrijmu.addDen(den);
    }

    void testAddDen() {
        assertFalse(kalPrijmu.hasData());
        kalPrijmu.addDen(den);
        assertTrue(kalPrijmu.hasData())
        kalPrijmu.clearZpracovaneDny();
    }

    void testToJSONObject() {

        JsonWriter writer = new JsonWriter();
        assertTrue(writer.save(kalPrijmu));
    }

    void testFromJson() {
        JSONObject obj = JsonReader.readKalendarPrijmu();
        assertNotNull(obj);
        KalendarPrijmu newKP = new KalendarPrijmu();
        newKP.fromJson(obj);
        assertTrue(newKP.hasData());
        println(newKP.toJSONObject().toJSONString());
    }
}
