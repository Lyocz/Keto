package cz.lyosoft.keto.model;

import cz.lyosoft.keto.logic.json.JsonObject;
import org.json.simple.JSONObject;

public class Porce implements JsonObject {

    private String nazev;
    private double gramy;
    private double tuky;
    private double bilkoviny;
    private double sacharidy;

    public double getGramy() {
        return gramy;
    }

    public void setGramy(double gramy) {
        this.gramy = gramy;
    }

    public double getTuky() {
        return tuky;
    }

    public void setTuky(double tuky) {
        this.tuky = tuky;
    }

    public double getBilkoviny() {
        return bilkoviny;
    }

    public void setBilkoviny(double bilkoviny) {
        this.bilkoviny = bilkoviny;
    }

    public double getSacharidy() {
        return sacharidy;
    }

    public void setSacharidy(double sacharidy) {
        this.sacharidy = sacharidy;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public boolean hasData() {
        return nazev != null && !nazev.isEmpty() && tuky >= 0.0 && bilkoviny >= 0.0 && sacharidy >= 0.0 && gramy > 0.0;
    }


    /**
     * ---------------   Json part ------------------------------------
     */

    @Override
    public JSONObject toJSONObject() {

        JSONObject jsonPorce = new JSONObject();
        jsonPorce.put("nazev", getNazev());
        jsonPorce.put("tuky", getTuky());
        jsonPorce.put("bilkoviny", getBilkoviny());
        jsonPorce.put("sacharidy", getSacharidy());
        jsonPorce.put("gramy", getGramy());

        return jsonPorce;
    }

    @Override
    public boolean fromJson(JSONObject jsonObject) {
        setNazev((String) jsonObject.get("nazev"));
        setTuky((Double) jsonObject.get("tuky"));
        setBilkoviny((Double) jsonObject.get("bilkoviny"));
        setSacharidy((Double) jsonObject.get("sacharidy"));
        if (jsonObject.containsKey("gramy")) {
            setGramy((Double) jsonObject.get("gramy"));
        }
        return hasData();
    }

}
