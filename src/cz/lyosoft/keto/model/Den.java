package cz.lyosoft.keto.model;

import cz.lyosoft.keto.logic.json.JsonObject;
import cz.lyosoft.keto.logic.util.DateTimeUtils;
import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Den implements JsonObject {


    DateTime datum;
    List<Porce> porces;

    public Den() {
        datum = new DateTime();
        porces = new ArrayList<>();
    }


    public Den(DateTime datum) {
        this.datum = datum;
    }

    public DateTime getDatum() {
        return datum;
    }

    public void setDatum(DateTime datum) {
        this.datum = datum;
    }

    public List<Porce> getPorces() {
        return porces;
    }

    public void addPorce(Porce porce) {
        this.porces.add(porce);
    }

    private boolean hasData() {
        return false;
    }


    @Override
    public JSONObject toJSONObject() {
        JSONObject obj = new JSONObject();
        obj.put("datum", DateTimeUtils.formatedDateString(datum));

        JSONArray array = new JSONArray();
        for (Porce p : porces) {
            array.add(p.toJSONObject());
        }

        obj.put("porces", array);
        return obj;
    }


    @Override
    public boolean fromJson(JSONObject jsonObject) {
        String name = (String) jsonObject.get("day");
        setDatum(new DateTime());
        System.out.println(name);

        List<Porce> list = new ArrayList<>();

        // loop array
        JSONArray lPorce = (JSONArray) jsonObject.get("porce");
        for (JSONObject aLPorce : (Iterable<JSONObject>) lPorce) {
            Porce porce = new Porce();
            JSONObject obj = aLPorce;
            porce.setNazev((String) obj.get("nazev"));
            porce.setTuky((double) obj.get("tuky"));
            porce.setBilkoviny((double) obj.get("bilkoviny"));
            porce.setSacharidy((double) obj.get("sacharidy"));
            addPorce(porce);
        }

        return hasData();
    }



    public String toString() {
        StringBuilder build = new StringBuilder();
        build.append("Date:").append(DateTimeUtils.formatedDateString(datum)).append("\n");
        for (Porce p : porces) {
            build.append(p.toJSONObject().toString()).append("\n");
        }
        return build.toString();
    }


    public void clearPorce() {
        porces.clear();
    }

    public void setPorces(List<Porce> porces) {
        this.porces = porces;
    }
}
