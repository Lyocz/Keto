package cz.lyosoft.keto.model

import cz.lyosoft.keto.logic.json.JsonWriter
import org.joda.time.DateTime

class DenTest extends GroovyTestCase {
    void testToJson() {
        Den den = new Den();
        den.setDatum(new DateTime())

        Porce p = new Porce();
        p.setTuky(10.0);
        p.setBilkoviny(8.0);
        p.setSacharidy(4.0);
        p.setNazev("Surovina 1");

        Porce p1 = new Porce();
        p1.setTuky(15.0);
        p1.setBilkoviny(7.0);
        p1.setSacharidy(2.0);
        p1.setNazev("Surovina 2");

        Porce p2 = new Porce();
        p2.setTuky(20.0);
        p2.setBilkoviny(18.0);
        p2.setSacharidy(6.0);
        p2.setNazev("Surovina 3");

        den.addPorce(p);
        den.addPorce(p1);
        den.addPorce(p2);

        assertNotNull(den.toJSONObject());
        assertTrue(JsonWriter.save(den));


    }
}
