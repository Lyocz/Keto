package cz.lyosoft.keto.model;

import cz.lyosoft.keto.logic.json.JsonObject;
import cz.lyosoft.keto.logic.json.JsonReader;
import cz.lyosoft.keto.logic.util.DateTimeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class KalendarPrijmu implements JsonObject {

    List<Den> zpracovaneDny;

    public KalendarPrijmu() {
        zpracovaneDny = new ArrayList<>();
        this.fromJson(JsonReader.readKalendarPrijmu());
        if (!exist(DateTimeUtils.getToday())) {
            zpracovaneDny.add(new Den());
        }
    }

    public boolean exist(String stDate) {
        boolean result = false;
        for (Den d : zpracovaneDny) {
            if (DateTimeUtils.formatedDateString(d.getDatum()).equals(stDate)) {
                result = true;
                break;
            }
        }
        return result;
    }


    /**
     * Prida den do seznamu zpracovanych dnu
     * a zaroven zaregistruje jeho datum pro pozdejsi praci s kalendarem
     *

     */
    protected void addDen(Den den) {
        zpracovaneDny.add(den);
    }

    protected void clearZpracovaneDny() {
        zpracovaneDny.clear();
    }

    public boolean hasData() {
        return zpracovaneDny.size() > 0;
    }


    public List<Den> getZpracovaneDny() {
        return zpracovaneDny;
    }


    public Den getToday() {
        return getDen(DateTimeUtils.getToday());
    }

    public Den getDen(String stDate) {
        Den den = null;
        for (Den d : zpracovaneDny) {
            if (DateTimeUtils.formatedDateString(d.getDatum()).equals(stDate))
                den = d;
        }

        return den;
    }


    @Override
    public JSONObject toJSONObject() {
        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();

        // Vsechny zpracovaneDny
        for (Den d : zpracovaneDny) {
            array.add(d.toJSONObject());
        }
        obj.put("zpracovaneDny", array);

        return obj;
    }

    @Override
    public boolean fromJson(JSONObject jsonObject) {
        if (jsonObject != null && !jsonObject.isEmpty()) {
            JSONArray array = (JSONArray) jsonObject.get("zpracovaneDny");
            for (Object dObj : array) {
                JSONObject jsonObj = (JSONObject) dObj;
                Den den = new Den();
                den.setDatum(DateTimeUtils.getDateTime(String.valueOf(jsonObj.get("datum"))));
                JSONArray jsonArray = (JSONArray) jsonObj.get("porces");
                for (Object pObj : jsonArray) {
                    JSONObject jpObj = (JSONObject) pObj;
                    Porce porce = new Porce();
                    porce.fromJson(jpObj);
                    den.addPorce(porce);
                }


                zpracovaneDny.add(den);
            }
        }

        return hasData();
    }


}
